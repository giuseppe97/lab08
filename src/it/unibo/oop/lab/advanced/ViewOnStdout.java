package it.unibo.oop.lab.advanced;

public class ViewOnStdout implements DrawNumberView {

    private DrawNumberViewObserver observer;

    @Override
    public void setObserver(DrawNumberViewObserver observer) {
        this.observer = observer;
    }

    @Override
    public void start() {
        System.out.println("Start the game");
    }

    @Override
    public void numberIncorrect() {
        System.out.println("wrong number");
    }

    @Override
    public void result(DrawResult res) {
        switch (res) {
        case YOURS_HIGH:
        case YOURS_LOW:
            System.out.println(res.getDescription());
            return;
        case YOU_WON:
            System.out.println(res.getDescription());
            break;
        default:
            throw new IllegalStateException("Unexpected result: " + res);
        }
    }

    @Override
    public void limitsReached() {
        System.out.println("You lost");
    }

    @Override
    public void displayError(String message) {
        System.out.println(message);
    }

}
