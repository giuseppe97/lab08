package it.unibo.oop.lab.advanced;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private static int MIN;
    private static int MAX;
    private static int ATTEMPTS;
    private final DrawNumber model;
    private final DrawNumberView mainView, fileView, outView;
    private final List<DrawNumberView> viewList;

    /**
     * @throws IOException 
     * 
     */
    public DrawNumberApp() throws IOException {
        final InputStream configuration = ClassLoader.getSystemResourceAsStream("config.yml");
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(configuration))) {
            String line;
            if((line = buffer.readLine()) != null) {
                final StringTokenizer token = new StringTokenizer(line, ": ");
                token.nextToken();
                MIN = Integer.parseInt(token.nextToken());
            }
            if((line = buffer.readLine()) != null) {
                final StringTokenizer token = new StringTokenizer(line, ": ");
                token.nextToken();
                MAX = Integer.parseInt(token.nextToken());
            }
            if((line = buffer.readLine()) != null) {
                final StringTokenizer token = new StringTokenizer(line, ": ");
                token.nextToken();
                ATTEMPTS = Integer.parseInt(token.nextToken());
            }
        }
        this.viewList = new LinkedList<>();
        this.model = new DrawNumberImpl(MIN, MAX, ATTEMPTS);
        this.fileView = new ViewOnFile();
        this.outView = new ViewOnStdout();
        this.mainView = new DrawNumberViewImpl();
        this.viewList.add(fileView);
        this.viewList.add(mainView);
        this.viewList.add(outView);
        for(final DrawNumberView tmp:this.viewList) {
            tmp.setObserver(this);
            tmp.start();
        }
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            for(final DrawNumberView tmp:this.viewList) {
                tmp.result(result);
            }
        } catch (IllegalArgumentException e) {
            for(final DrawNumberView tmp:this.viewList) {
                tmp.numberIncorrect();
            }
        } catch (AttemptsLimitReachedException e) {
            for(final DrawNumberView tmp:this.viewList) {
                tmp.limitsReached();
            }
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     * @throws IOException 
     */
    public static void main(final String... args) throws IOException {
        new DrawNumberApp();
    }

}
