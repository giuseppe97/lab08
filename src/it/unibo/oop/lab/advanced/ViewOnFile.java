package it.unibo.oop.lab.advanced;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ViewOnFile implements DrawNumberView {
    
    private DrawNumberViewObserver observer;
    private final File file;
    
    public ViewOnFile() {
        this.file = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "number.txt");
    }

    @Override
    public void setObserver(DrawNumberViewObserver observer) {
        this.observer = observer;
    }

    @Override
    public void start() {
        try(BufferedWriter buffer = new BufferedWriter(new FileWriter(this.file))) {
            buffer.write("Start the game \n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void numberIncorrect() {
        try(BufferedWriter buffer = new BufferedWriter(new FileWriter(this.file))) {
            buffer.write("wrong number \n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void result(DrawResult res) {
        switch (res) {
        case YOURS_HIGH:
        case YOURS_LOW:
            try(BufferedWriter buffer = new BufferedWriter(new FileWriter(this.file))) {
                buffer.write(res.getDescription() + "\n");
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            return;
        case YOU_WON:
            try(BufferedWriter buffer = new BufferedWriter(new FileWriter(this.file))) {
                buffer.write(res.getDescription() + "\n");
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            break;
        default:
            throw new IllegalStateException("Unexpected result: " + res);
        }
    }

    @Override
    public void limitsReached() {
        try(BufferedWriter buffer = new BufferedWriter(new FileWriter(this.file))) {
            buffer.write("You lost \n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void displayError(String message) {
        try(BufferedWriter buffer = new BufferedWriter(new FileWriter(this.file))) {
            buffer.write("An error has occurred \n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
