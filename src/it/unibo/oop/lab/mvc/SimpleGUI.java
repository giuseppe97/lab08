package it.unibo.oop.lab.mvc;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    public static void main(String...strings) {
        new SimpleGUI();
    }
    
    private final JFrame frame = new JFrame();

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        final JPanel panel = new JPanel(new BorderLayout());
        this.frame.getContentPane().add(panel);
        final JTextField textField = new JTextField("tfield");
        panel.add(textField, BorderLayout.NORTH);
        final JTextArea textArea = new JTextArea();
        panel.add(textArea, BorderLayout.CENTER);
        final JButton print = new JButton("Print");
        final JButton button = new JButton("Show history");
        panel.add(print, BorderLayout.WEST);
        panel.add(button, BorderLayout.EAST);
        final IOController controller = new IOController();
        print.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                    controller.setToPrint(textField.getText());
                    controller.printCurrent();
            }
        });
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for(final String string:controller.getHistory()) {
                    textArea.append(string);
                    textArea.append("\n");
                }
            }
        });
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
}
