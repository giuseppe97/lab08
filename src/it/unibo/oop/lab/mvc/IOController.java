package it.unibo.oop.lab.mvc;
import java.io.IOException;
import java.util.*;

public class IOController implements Controller {

    private String current;
    private final List<String> list;
    
    public IOController() {
        this.current = new String();
        this.list = new LinkedList<String>();
    }
    
    public void setToPrint(String string) throws IllegalStateException {
        if(string==null) {
            throw new IllegalStateException();
        }
        this.current = string;
        list.add(string);
    }

    public String getToPrint() {
        return this.current;
    }

    public List<String> getHistory() {
        return this.list;
    }

    public void printCurrent() throws IllegalStateException {
        if (this.current == null) {
            throw new IllegalStateException();
        }
        System.out.println(this.current);
    }

}
