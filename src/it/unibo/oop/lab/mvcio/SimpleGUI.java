package it.unibo.oop.lab.mvcio;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * A very simple program using a graphical interface.
 * 
 */
public class SimpleGUI {
    
    private final JFrame frame = new JFrame();
    
    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        
        final JPanel panel = new JPanel(new BorderLayout());
        this.frame.getContentPane().add(panel);
        final JTextArea textarea = new JTextArea("Insert here");
        panel.add(textarea, BorderLayout.CENTER);
        final JButton button = new JButton("Save");
        panel.add(button, BorderLayout.SOUTH);
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setLocationByPlatform(true);
        this.frame.setVisible(true);
        
        button.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                try {
                    new Controller().writeOnFile(textarea.getText());
                } catch (IOException ioe) {
                    System.out.println(ioe.getMessage());
                }
            }
        });
    }
    
    /**
     * the main.
     * @param strings
     * tag
     */
    public static void main(final String...strings) {
        new SimpleGUI();
    }
}









