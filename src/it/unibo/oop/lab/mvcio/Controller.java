package it.unibo.oop.lab.mvcio;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Controller {
    
    private File current;
    
    public Controller() {
        this.current = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "output.txt");
    }
    
    public void setCurrent(final File file) {
        this.current = file;
    }
    
    public File getCurrent() {
        return this.current;
    }
    
    public String getPath() {
        return this.current.getPath();
    }
    
    public void writeOnFile(final String str) throws IOException {
        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(current)))) {
            out.writeUTF(str + "\n");
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }
    
}















