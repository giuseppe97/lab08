package it.unibo.oop.lab.iogui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Random;
import java.util.StringTokenizer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * This class is a simple application that writes a random number on a file.
 * 
 * This application does not exploit the model-view-controller pattern, and as
 * such is just to be used to learn the basics, not as a template for your
 * applications.
 */
public class BadIOGUI {

    private static final String TITLE = "A very simple GUI application";
    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + BadIOGUI.class.getSimpleName() + ".txt";
    private final Random rng = new Random();
    private final JFrame frame = new JFrame(TITLE);

    /**
     * 
     */
    public BadIOGUI() {
        final JPanel canvas = new JPanel();
        canvas.setLayout(new BorderLayout());
        final JPanel newpan = new JPanel();
        newpan.setLayout(new BoxLayout(newpan, BoxLayout.X_AXIS));
        final JButton write = new JButton("Write on file");
        newpan.add(write);
        canvas.add(newpan, BorderLayout.CENTER);
        final JButton newButton = new JButton("Read");
        newpan.add(newButton);
        frame.setContentPane(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        newButton.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                try (BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(PATH)))) {
                    System.out.println(input.readLine());
                } catch (Exception err) {
                    System.out.println(err.toString());
                }
            }
        });
        
        write.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                /*
                 * This would be VERY BAD in a real application.
                 * 
                 * This makes the Event Dispatch Thread (EDT) work on an I/O
                 * operation. I/O operations may take a long time, during which
                 * your UI becomes completely unresponsive.
                 */
                try (PrintStream ps = new PrintStream(PATH)) {
                    ps.print(rng.nextInt());
                } catch (FileNotFoundException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });
    }

    private void display() {
        /*
         * Make the frame one fifth the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected. In order to deal coherently with multimonitor
         * setups, other facilities exist (see the Java documentation about this
         * issue). It is MUCH better than manually specify the size of a window
         * in pixel: it takes into account the current resolution.
         *
         *
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.pack();
        frame.setLocationByPlatform(true);
        /*
         * OK, ready to pull the frame onscreen
         */
        frame.setVisible(true);
    }

    /**
     * @param args ignored
     * @throws IOException 
     */
    public static void main(final String... args) throws IOException {
        final InputStream configuration = ClassLoader.getSystemResourceAsStream("config.yml");
        try(final BufferedReader buffer = new BufferedReader(new InputStreamReader(configuration))) {
            String line;
            while((line = buffer.readLine()) != null) {
                final StringTokenizer token = new StringTokenizer(line, ":");
                token.nextToken();
                System.out.println(token.nextToken());
            }
            new BadIOGUI().display();
        }
    }
}






















