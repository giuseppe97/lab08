package it.unibo.oop.lab.mvcio2;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;
import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
    
    public static void main(String...strings) {
        new SimpleGUIWithFileChooser();
    }
    
    private final JFrame frame = new JFrame();
    
    public SimpleGUIWithFileChooser() {
        
        final Controller controller = new Controller();
        final JPanel panel = new JPanel(new BorderLayout());
        this.frame.getContentPane().add(panel);
        final JTextArea textarea = new JTextArea("Insert here");
        panel.add(textarea, BorderLayout.CENTER);
        final JButton button = new JButton("Save");
        panel.add(button, BorderLayout.SOUTH);
        final JPanel newPanel = new JPanel(new BorderLayout());
        final JTextField textField = new JTextField(controller.getPath());
        newPanel.add(textField, BorderLayout.NORTH);
        final JButton newButton = new JButton("Browse...");
        newPanel.add(newButton, BorderLayout.LINE_END);
        panel.add(newPanel, BorderLayout.NORTH);
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setLocationByPlatform(true);
        this.frame.setVisible(true);
        
        newButton.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser filechooser = new JFileChooser();
                if (filechooser.showSaveDialog(filechooser) == JFileChooser.APPROVE_OPTION) {
                    controller.setCurrent(filechooser.getSelectedFile());
                    textField.setText(controller.getPath());
                } else if (filechooser.showSaveDialog(filechooser) == JFileChooser.CANCEL_OPTION) {
                    JOptionPane.showMessageDialog(filechooser, "An error has occurred", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        button.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                try {
                    controller.writeOnFile(textarea.getText());
                } catch (IOException ioe) {
                    System.out.println(ioe.getMessage());
                }
            }
        });
    }
    
}
